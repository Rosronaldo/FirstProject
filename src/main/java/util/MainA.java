package util;

import config.Config;
import domain.Phone;
import domain.Producers;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service.PhoneService;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ������������� on 29.10.2015.
 */
public class MainA {

    public static void main(String[] args) {

//        ApplicationContext context = new ClassPathXmlApplicationContext("app-context.xml");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        PhoneService phoneService = (PhoneService) context.getBean("phoneServiceImpl");

        Phone phone11 = new Phone();
        phone11.setName("SM-G313H Galaxy Ace 4 Lite ZKA");
        phone11.setPrice(1039);
        phone11.setQuantity(5);
        phone11.setMargin(400);

        Phone phone12 = new Phone();
        phone12.setName("SM-G313H Galaxy");
        phone12.setPrice(1339);
        phone12.setQuantity(5);
        phone12.setMargin(500);

        Phone phone21 = new Phone();
        phone21.setName("LG Optimus L90 Dual D410 White");
        phone21.setPrice(2899);
        phone21.setQuantity(5);
        phone21.setMargin(300);

        Phone phone22 = new Phone();
        phone22.setName("LG Optimus L90 Dual D410 Black");
        phone22.setPrice(2899);
        phone22.setQuantity(5);
        phone22.setMargin(300);

        Phone phone31 = new Phone();
        phone31.setName("Apple iPhone 5s 16GB Gold(CPO)");
        phone31.setPrice(9399);
        phone31.setQuantity(5);
        phone31.setMargin(1000);

        Phone phone32 = new Phone();
        phone32.setName("Apple iPhone 5s 16GB Silver");
        phone32.setPrice(8100);
        phone32.setQuantity(5);
        phone32.setMargin(1000);

        Set<Phone> phone1 = new HashSet<Phone>();
        phone1.add(phone11);
        phone1.add(phone12);

        Set<Phone> phone2 = new HashSet<Phone>();
        phone2.add(phone11);
        phone2.add(phone12);

        Set<Phone> phone3 = new HashSet<Phone>();
        phone3.add(phone11);
        phone3.add(phone12);

        Producers producer1 = new Producers();
        producer1.setProducer("Samsung");
        producer1.setPhone(phone1);

        Producers producer2 = new Producers();
        producer2.setProducer("LG");
        producer2.setPhone(phone2);

        Producers producer3 = new Producers();
        producer3.setProducer("Apple");
        producer3.setPhone(phone3);

        phone11.setProducers(producer1);
        phone12.setProducers(producer1);
        phone21.setProducers(producer2);
        phone22.setProducers(producer2);
        phone31.setProducers(producer3);
        phone32.setProducers(producer3);

//        PhoneService phoneService = new PhoneServiceImpl();

        phoneService.addProducers(producer1);
        phoneService.addProducers(producer2);
        phoneService.addProducers(producer3);

        phoneService.addPhone(phone11);
        phoneService.addPhone(phone12);
        phoneService.addPhone(phone21);
        phoneService.addPhone(phone22);
        phoneService.addPhone(phone31);
        phoneService.addPhone(phone32);

        phoneService.sell();

    }
}
