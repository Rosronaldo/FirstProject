package util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

/**
 * Created by ������������� on 15.11.2015.
 */
public class HibernateUtil {

    /**
     * Created by ?����������� on 24.10.2015.
     */
        private static final SessionFactory sessionFactory = buildSessionFactory();
        private static SessionFactory buildSessionFactory() {

            try {
                return new Configuration().configure(
                        new File("hibernate.cfg.xml")).buildSessionFactory();
            } catch (Throwable ex) {
                System.err.println("Initial SessionFactory creation failed." + ex);
                throw new ExceptionInInitializerError(ex);
            }
        }

        public static SessionFactory getSessionFactory() {return sessionFactory;}

        public static void shutdown() {
            getSessionFactory().close();
        }

    }


