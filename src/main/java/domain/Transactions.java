package domain;

import javax.persistence.*;

/**
 * Created by ������������� on 24.10.2015.
 */

@Entity
public class Transactions {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @Column(length = 500)
    private String name;
    private String producer;
    private int profit;

    public Transactions() {
    }


    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

}
