package domain;

import javax.persistence.*;

/**
 * Created by ������������� on 24.10.2015.
 */
@Entity
public class Phone {
    @Id
    @GeneratedValue
    private Long PHONE_ID;

    @Column(length = 500)
    private String name;
//    private String producer;
//    private enum manufacturer {LG, Samsung, IPhohne};
    private int price;
    private int quantity;
    private int margin;

//    @OneToMany (mappedBy = "phone")
//    private Set <Producers> producers = new HashSet<Producers>();

    @ManyToOne
    @JoinColumn(name = "Id")
    private Producers producers;


    public Phone() {
    }

    public void setProducers(Producers producers) {
        this.producers = producers;
    }

    public Long getPHONE_ID() {
        return PHONE_ID;
    }

    public void setPHONE_ID(Long PHONE_ID) {
        this.PHONE_ID = PHONE_ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

//    public String getProducer() {
//        return producer;
//    }
//
//    public void setProducer(String producer) {
//        this.producer = producer;
//    }

    public String getName() {
        return name;
    }

   public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getMargin() {
        return margin;
    }

    public Producers getProducers() {
        return producers;
    }

//    public Set<Producers> getProducers() {
//        return producers;
//    }

//    public Set getProducers() {return producers;}
//    public void setProducers(Set<Producers> producers) {
//        this.producers = producers;
//    }
}
