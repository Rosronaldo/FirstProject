package domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ������������� on 01.11.2015.
 */
@Entity
public class Producers {

    @Id
    @GeneratedValue
    private Long Id;
    private String producer;

    public Producers() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

//    @ManyToOne
//    @JoinColumn(name = "PHONE_ID")
//    private Phone phone;

    @OneToMany (mappedBy = "producers")
    private Set<Phone> phone = new HashSet<Phone>();

    public void setPhone(Set<Phone> phone) {
        this.phone = phone;
    }

//    public Phone getPhone() {
//        return phone;
//    }
//
//    public void setPhone(Phone phone) {
//        this.phone = phone;
//    }


    public Set<Phone> getPhone() {
        return phone;
    }
}
