package dao;

import domain.Transactions;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import util.HibernateUtil;

/**
 * Created by ������������� on 31.10.2015.
 */
@Repository
public class TransactionJPADao implements TransactionDao {

   public void addTrans (Transactions transactions) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(transactions);
        session.getTransaction().commit();
        session.close();

    };

}
