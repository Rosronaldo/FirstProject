package dao;

import domain.Phone;
import domain.Producers;

/**
 * Created by ������������� on 24.10.2015.
 */
public interface PhoneDao {

    public void add (Producers producers);
    public void add (Phone phone);
    public void sell (Phone phone);
    public Phone read (Long Id);
    public void update (Phone phone);

}
