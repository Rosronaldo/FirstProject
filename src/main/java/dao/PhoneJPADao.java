package dao;

import domain.Phone;
import domain.Producers;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import util.HibernateUtil;

/**
 * Created by ������������� on 24.10.2015.
 */

@Repository
@Qualifier(value = "phoneJPADao")
public class PhoneJPADao implements PhoneDao{

    public PhoneJPADao() {
        System.out.println("Bean BookJPADao was initialized");
    }

    public void add(Producers producers) {
        Session session = HibernateUtil.getSessionFactory().openSession();

       try {
           session.beginTransaction();
           session.save(producers);
           session.getTransaction().commit();
       }catch (Exception e) {
           System.out.println(e);
       }finally {
           session.close();
       }

//        HibernateUtil.shutdown();
    }

    public void add(Phone phone) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(phone);
        session.getTransaction().commit();
        session.close();
        System.out.println ("Phone '" + phone.getName() + "' was added.");
//        HibernateUtil.shutdown();
    }

    public void sell(Phone phone) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(phone);
        session.getTransaction().commit();
        session.close();
        System.out.println ("Phone '" + phone.getName() + "' was sold.");
    }

    public Phone read (Long Id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Phone phone;
        phone = (Phone) session.get(Phone.class, Id);
//      session.getTransaction().commit();
        session.close();
        return phone;
    }

    public void update(Phone phone) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(phone);
        session.getTransaction().commit();
        session.close();
        System.out.println ("Phone '" + phone.getName() + "' was sold.");
    }


}
