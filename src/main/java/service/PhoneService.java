package service;

import domain.Phone;
import domain.Producers;

/**
 * Created by ������������� on 31.10.2015.
 */
public interface PhoneService {

    void addProducers (Producers producers);
    void addPhone (Phone phone);
    void sell();
}
