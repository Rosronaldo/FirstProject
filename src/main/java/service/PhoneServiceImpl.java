package service;

import dao.PhoneDao;
import dao.TransactionDao;
import dao.TransactionJPADao;
import domain.Phone;
import domain.Producers;
import domain.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by ������������� on 31.10.2015.
 */
@Service
public class PhoneServiceImpl implements PhoneService {

    @Autowired
    @Qualifier(value = "phoneJPADao")
    private PhoneDao phoneDao;

    @Autowired
    private TransactionDao transactionDao;

    private Phone phone;
    private Transactions transactions;

//    public void setPhoneDao (PhoneDao phoneDao)
//    {
//    this.phoneDao = phoneDao;
//    }

//    public void setTransactionDao (TransactionDao transactionDao)
//    {
//        this.transactionDao = transactionDao;
//    }

    public PhoneServiceImpl() {
//        PhoneDao phoneDao = new PhoneJPADao();
//        this.phoneDao = phoneDao;
//        PhoneDao phoneDao1 = new TransactionJPADao();
        System.out.println("Bean PhoneServiceImpl was initialized");
    }

public void addPhone (Phone phone){
//    PhoneDao phoneDao = new PhoneJPADao();
    phoneDao.add(phone);
}

public void addProducers (Producers producers){
//        PhoneDao phoneDao = new PhoneJPADao();
        phoneDao.add(producers);
}


public void sell (){
//    PhoneDao phoneDao = new PhoneJPADao();
    TransactionDao transactionDao = new TransactionJPADao();
//    Long Id_max=6L;
//    Long Id;
    MyThread one = new MyThread();
    one.start();

    long x = 4L;
    long y = 10L;
    int numEntry = 6;
        while (numEntry>0) {

        do {
            Random r = new Random();
            long number = x + ((long) (r.nextDouble() * (y - x)));
            phone = phoneDao.read(number);
        } while (phone == null);

        if (phone.getQuantity()==1)
        {phoneDao.sell(phone);
            numEntry = numEntry - 1;
        }
        else {
            phone.setQuantity(phone.getQuantity()-1);
            phoneDao.update(phone);
        }

        Transactions transactions = new Transactions();
        transactions.setName(phone.getName());
        transactions.setProfit(phone.getPrice() + phone.getMargin());

        Producers producers = phone.getProducers();
        transactions.setProducer(producers.getProducer());
        transactionDao.addTrans(transactions);

        try {
            one.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
}

class MyThread extends Thread {

    @Override
    public void run() {
        // TODO Auto-generated method stub
    }
}
